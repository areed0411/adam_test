#Thumb Print Scanner
import sys
#Header - 2 Bytes Fixed value of 0xEF01
#Adder (Address) - 4 Bytes Default value of 0xFFFFFFFF
#Package ID (PID) - 1 Byte: 0x01 Command Packet, 0x02 Data Packet, 0x07 Ack packet, 0x08 End of Data Packet
#Package Length LENGTH - 2 bytes - Length of data and checksum
#Package Contents DATA - Variable length Commands, data, Command Parameters, ack, ect.
#Check Sum - 2 bytes Arithmetic sum of package identifier, package length, and all package contents. Overflow bits are ommitted. High byte is transferred first.
#DEfault Password = 0x00000000

#SUM = PID + LENGTH + DATA
import Thumb_Const as codes
import serial
import time




ser = serial.Serial (codes.SER_PORT, codes.SER_RATE, codes.SER_BYTESIZE, codes.SER_PARITY, codes.SER_STOPBITS, codes.SER_TIMEOUT, codes.SER_XONXOFF, codes.SER_RTSCTS)

def main():
    ser.flush()
    ser.close()
    ser.open()
    ScanFinger()

def TurnOnLED():
    print("Turning LED On")
    ser.write(OpenLighting_Packet())
    readFrom = RECV_PCKT()
    print("Turn on LED response", readFrom)

def TurnOffLED():
    print("Turning LED Off")
    ser.write(CloseLighting_Packet())
    readFrom = RECV_PCKT()
    print("Turn off LED response", readFrom)

def BlinkLED():
    FF=True
    while True:

        if FF:
            print("LED On")
            ser.write(OpenLighting_Packet())
            readFrom = RECV_PCKT()
            print(readFrom)
            FF = False
        else:
            print("LED off")
            ser.write(CloseLighting_Packet())
            readFrom = RECV_PCKT()
            print(readFrom)
            FF = True
        time.sleep(2.5)

def ScanFinger():
    print("\n\nScanning Finger And Storing in Buffer")
    while True:
        TurnOnLED()
        ser.write(GetImg_Packet())
        responsePCKT = RECV_PCKT()
        responseCode = responsePCKT[3]
        print("Get Finger Response: ", responsePCKT)
        TurnOffLED()
        print(responseCode)
        time.sleep(1)
        if responseCode == int.from_bytes(codes.ACK_COMMAND_EXECUTION_COMPLETE, byteorder="big"):
            break
        else:
            print("Failed To Obtain Finger. Retrying...")
    Print("Sucessfully attained finger and stored in IMG Buffer")


#Verify the Password/Login
def Verify_Password_Packet():
    return codes.HEADER + codes.ADDRESS + codes.PACKET_COMMAND + codes.SIZE_VERIFY_PASSWORD + codes.DATA_VERIFY_PASSWORD + codes.CSUM_VERIFY_PASSWORD

#detecting finger and store the detected finger image in ImageBuffer while
#returning successful confirmation code; If there is no finger, returned
#confirmation code would be” can’t detect finger” .
def GetImg_Packet():
    return codes.HEADER + codes.ADDRESS + codes.PACKET_COMMAND + codes.SIZE_GETIMG + codes.DATA_GETIMG + codes.CSUM_GETIMG

def OpenLighting_Packet():
    return codes.HEADER + codes.ADDRESS + codes.PACKET_COMMAND + codes.SIZE_LIGHTING_OPEN + codes.CODE_LIGHTING_OPEN + codes.CSUM_LIGHTING_OPEN

def CloseLighting_Packet():
    return codes.HEADER + codes.ADDRESS + codes.PACKET_COMMAND + codes.SIZE_LIGHTING_CLOSE + codes.CODE_LIGHTING_CLOSE + codes.CSUM_LIGHTING_CLOSE

def GetImg_Free_Lighting_Packet():
    return codes.HEADER + codes.ADDRESS + codes.PACKET_COMMAND + codes.SIZE_GETIMG_FREE + codes.CODE_GETIMG_FREE + codes.CSUM_GETIMG_FREE



#Convert the image stored in the image buffer into a character file stored in BUFFER 1
def Img2tz_BUFONE_Packet():
    return codes.HEADER + codes.ADDRESS + codes.PACKET_COMMAND + codes.DATA_IMG2TZ_CHR1

#Convert the image stored in the image buffer into a character file stored in BUFFER 2
def Img2tz_BUFTWO_Packet():
    return codes.HEADER + codes.ADDRESS + codes.PACKET_COMMAND + codes.DATA_IMG2TZ_CHR2


#Echo the device to check for functionality/response
def Echo_Packet():
    return codes.HEADER + codes.ADDRESS + codes.PACKET_COMMAND + codes.SIZE_ECHO + codes.CODE_ECHO + codes.CSUM_ECHO


#Upload the character file to the computer
def UploadChar_Packet(Buffer):
    if Buffer == 1:
        return codes.HEADER + codes.ADDRESS + codes.PACKET_COMMAND + codes.DATA_UPCHAR_CHRONE
    else:
        return codes.HEADER + codes.ADDRESS + codes.PACKET_COMMAND + codes.DATA_UPCHAR_CHRTWO

#Write a packet on the serial connection
def SendPacket(packet):
    ser.write(packet)

#Wait for a response from the scanner
def Wait_For_Response(responseSize):
    waitTime = 0
    reply = [0]
    while not ser.readable():
        time.sleep(0.1)
        waitTime = waitTime + 1
        if waitTime > 10:
            return -1
    while reply[0] == 0:
        reply[0] = ser.read(1)

    reply.extend( ser.read(responseSize-1) )
    return reply

def RECV_PCKT():
    waitTime = 0
    reply = [0]
    while not ser.readable():
        time.sleep(0.1)
        waitTime = waitTime + 1
        if waitTime > 10:
            return -1

    while reply[0] == 0:
        reply[0] = ser.read(7) #Keep reading until header is found

    #reply.extend( ser.read(5) ) #Read ADDRESS + Identifier
    reply.extend( ser.read(2) ) #Read Package Length
    PacketLength = reply[2] - 2
    print("Packet Length = ", PacketLength)
    reply.extend( ser.read(PacketLength) ) #Read Packet Length
    reply.extend( ser.read(2) ) #Read checksum
    return reply

if __name__ == "__main__":
    main()
